# labime

Laboratoire d'imprimerie physique et numérique libre, labime est un projet atelier mené au sein du BIB Hackerspace à Montpellier.

labime explore les possibles et perspectives des arts de l'imprimerie avec les moyens du bord.

